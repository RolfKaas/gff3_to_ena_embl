# README #

### Introduction ###

This tool converts a gff3 annotation file, with sequence, into an embl formatted file, ready for submission to the public European Nucleotide Archive ([ENA](https://www.ebi.ac.uk/ena)).  

The tool is essentially just a wrapper around seqret, it does the following:

1. Extract all sequence into a FASTA formatted file.
2. Splits the gff3 file up with respect to contigs.
3. The sequence data from the FASTA file is then attached to their respective gff3 files.
4. [seqret](http://emboss.sourceforge.net/apps/release/6.1/emboss/apps/seqret.html) converts each gff3 file to an embl formatted file.
5. The header of each embl formatted file is replaced with an ENA appropriate header.
6. All the individual embl formatted files are concatenated into one and written as output.
7. The [ENA validation tool](http://www.ebi.ac.uk/ena/software/flat-file-validator) checks the output file.

**Note:** The testing of this tools is very limited and has only been done on gff3 files created by the [PROKKA](http://www.vicbioinformatics.com/software.prokka.shtml) pipeline version 1.11.

**Alternative** After the release of this script another more robust tool was developed by NBIS in Sweden, [EMBLmyGFF3](https://github.com/NBISweden/EMBLmyGFF3).

### Dependencies ###

* [Python 3](https://www.python.org/)
* seqret part of [EMBOSS](http://emboss.sourceforge.net/download/)
* [Java](https://www.java.com/en/download/)
* [embl-api-validator](http://www.ebi.ac.uk/ena/software/flat-file-validator)

### Installation ###

#### Clone the repository ####
The --recursive flag is needed when you clone this repository.

~~~
git clone --recursive https://bitbucket.org/RolfKaas/gff3_to_ena_embl.git
~~~
#### Edit the config file ####
Tell the tool where you keep the different external dependencies by editing the file "gff3_to_ena_embl_config.txt".

The format of the config file:

* Lines starting with hashtags are ignored.
* The dependencies are written as: name<tab>path.
* The names must not be changed.
* The path can be links or absolute paths.

*gff3_to_ena_embl_config.txt:*
~~~
# Configuration file for gff3_to_ena_embl
# Host: My Mac
seqret	/usr/local/bin/seqret
embl-api-validator	/Users/rolf/bioinf_apps/embl-api-validator-1.1.156.jar
java	/usr/bin/java
~~~

### Running the tool ###

#### Example ####

~~~
python3 gff3_to_ena_embl.py --genome_type linear --project_acc PRJEB19208 \
--authors 'Rolf Sommer Kaas, Hanne Mordhorst, Pimlapas Leekitcharoenphon, Jacob\
 Dyring Jensen, Janus A. J. Haagensen, S�ren Molin, S�nje Johanna Pamp' --title \
 'Draft Genome Sequence of Acinetobacter johnsonii C6, an Environmental Isolate \
 Participating in Interspecific Metabolic Interactions' --submission_date \
 04-Feb-2017 --taxon_id 40214 --organism 'Acinetobacter johnsonii' --output \
 ACNJC6_01052017.embl --temp_dir dir_will_be_del ACNJC6_01052017.gff
~~~

#### Help document ####
~~~
usage: gff3_to_ena_embl.py [-h] [-o OUTPUT] [-g GENOME_TYPE] [-p ACCNO]
                           [-a STRING] [-t STRING] [-s STRING] [-ta INT]
                           [-org STRING] [-c CONF] [--temp_dir TEMP_DIR]
                           GFF3

The script was created to turn gff3 files created with PROKKA into embl
formatted files usable for submission to ENA.

positional arguments:
  GFF3                  Input file to convert.

optional arguments:
  -h, --help            show this help message and exit
  -o OUTPUT, --output OUTPUT
                        Name of output file
  -g GENOME_TYPE, --genome_type GENOME_TYPE
                        Must either 'linear' or 'circular'
  -p ACCNO, --project_acc ACCNO
                        Accession number of project.
  -a STRING, --authors STRING
                        Author names enclosed in quotes and seperated by
                        commas.
  -t STRING, --title STRING
                        Title of paper.
  -s STRING, --submission_date STRING
                        Date of submission. Must conform to the format: DD-
                        MMM-YYYY
  -ta INT, --taxon_id INT
                        Taxon ID. These can be found at EBI or NCBI.
  -org STRING, --organism STRING
                        Genus and species.
  -c CONF, --config CONF
                        Path to configuration file.
  --temp_dir TEMP_DIR   Set location of the temporary directory. It will be
                        deleted after execution.
~~~