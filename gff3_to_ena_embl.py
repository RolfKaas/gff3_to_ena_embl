#! /tools/bin/python3

import argparse
import os.path
import re
import shutil
from signal import *
import tempfile
import sys
import subprocess
import urllib.parse

from Dependencies import Dependencies
from FastaTools import Metadata

#
# Clean up temporary directory if killed.
#


def clean(*args):
    global temp_dir

    if(temp_dir):
        print("!! Removing temp directory: " + temp_dir + "...")
        shutil.rmtree(temp_dir)
        print("!! \tTemporary directory removed.")

    os._exit(0)

for sig in (SIGABRT, SIGINT, SIGTERM):
    signal(sig, clean)


def widthformat(text, max_width=80, line_prefix=""):
    ''' Takes a string and returns a string that is formatted to max_width
        characters and each line will be prefixed with line_prefix, the
        line_prefix can be empty.
        default max_width: 80.
        default line_prefix: ""
    '''
    string_list = text.split(" ")
    first_word = string_list[0]
    del(string_list[0])

    output_text = line_prefix + first_word
    current_width = len(output_text)

    for word in string_list:
        if(current_width + len(word) > max_width-1):
            output_text += "\n" + line_prefix + word
            current_width = len(line_prefix) + len(word)
        else:
            output_text += " " + word
            current_width += 1 + len(word)

    return output_text


def get_header(genome_type, seq_region, start, end, acc_no, authors, title,
               date, organism, taxon_id):

    author_entry = widthformat(authors, line_prefix="RA   ")
    title_entry = widthformat(title, line_prefix="RT   ")

    header = ("ID   XXX; XXX; " + genome_type + "; XXX; XXX; XXX; XXX.\n" +
              "XX\n" +
              "AC   XXX;\n" +
              "XX\n" +
              "AC * _" + seq_region + "\n" +
              "XX\n" +
              "PR   Project:" + acc_no + ";\n" +
              "XX\n" +
              "DE   XXX\n" +
              "XX\n" +
              "RN   [1]\n" +
              author_entry + ";\n" +
              title_entry + ";\n" +
              "RL   Submitted (" + date + ") to the INSDC.\n" +
              "XX\n" +
              "FH   Key             Location/Qualifiers\n" +
              "FH\n" +
              "FT   source          " + str(start) + ".." + str(end) + "\n" +
              "FT                   /organism=\"" + organism + "\"\n" +
              "FT                   /mol_type=\"genomic DNA\"\n" +
              "FT                   /db_xref=\"taxon:"+str(taxon_id) + "\"\n" +
              "FT                   /note=\"" + seq_region + "\"\n")
    return header


if __name__ == '__main__':

    #
    # Handling arguments
    #
    parser = argparse.ArgumentParser(description="The script was created to\
        turn gff3 files created with PROKKA into embl formatted files usable\
        for submission to ENA.")
    # Posotional arguments
    parser.add_argument("gff3_file",
                        help="Input file to convert.",
                        metavar='GFF3',
                        default=None)
    # Optional arguments
    parser.add_argument("-o", "--output",
                        help="Name of output file",
                        default=None,
                        metavar="OUTPUT")
    parser.add_argument("-g", "--genome_type",
                        help="Must either 'linear' or 'circular'",
                        metavar='GENOME_TYPE',
                        choices=["linear", "circular"],
                        default="circular")
    parser.add_argument("-p", "--project_acc",
                        help="Accession number of project.",
                        default="",
                        metavar="ACCNO")
    parser.add_argument("-a", "--authors",
                        help="Author names enclosed in quotes and seperated by\
                              commas.",
                        default=None,
                        metavar="STRING")
    parser.add_argument("-t", "--title",
                        help="Title of paper.",
                        default=None,
                        metavar="STRING")
    parser.add_argument("-s", "--submission_date",
                        help="Date of submission. Must conform to the format:\
                              DD-MMM-YYYY",
                        default=None,
                        metavar="STRING")
    parser.add_argument("-ta", "--taxon_id",
                        help="Taxon ID. These can be found at EBI or NCBI.",
                        default=None,
                        type=int,
                        metavar="INT")
    parser.add_argument("-org", "--organism",
                        help="Genus and species.",
                        default=None,
                        metavar="STRING")
    parser.add_argument("-c", "--config",
                        help="Path to configuration file.",
                        metavar="CONF")
    parser.add_argument("--temp_dir",
                        help="Set location of the temporary directory. It will\
                              be deleted after execution.",
                        metavar='TEMP_DIR',
                        default=None)

    args = parser.parse_args()

    # Check configuration files.
    if(not args.config):
        args.config = os.path.dirname(
            os.path.realpath(__file__)) + "/gff3_to_ena_embl_config.txt"
    if(not os.path.isfile(args.config)):
        print("Configuration file not found:", args.config)
        quit(1)

    # Create temporary directory for the temporary files.
    temp_dir = None
    if(args.temp_dir):
        try:
            shutil.rmtree(args.temp_dir)
        except:
            pass
        os.makedirs(args.temp_dir, exist_ok=True)
        temp_dir = args.temp_dir
    else:
        temp_dir = tempfile.mkdtemp(prefix='gff3toembl')

    # Load dependencies
    prgs = Dependencies(args.config, executables=False)

    re_seq_reg = re.compile(r"^##sequence-region\s+(\S+)\s+(\d+)\s+(\d+)")
    re_fasta_header = re.compile(r"^>(\S+)")
    seq_regions = {}
    tmp_fasta_file = temp_dir + "/fasta"

    with open(args.gff3_file, "r") as fh:
        for line in fh:
            match_seq_reg = re_seq_reg.search(line)

            # Load sequence regions.
            if(match_seq_reg):
                reg_id = match_seq_reg.group(1)
                start = match_seq_reg.group(2)
                end = match_seq_reg.group(3)
                seq_regions[reg_id] = (start, end)

            # Load FASTA sequences.
            elif(line.startswith("##FASTA")):
                with open(tmp_fasta_file, "w", encoding="utf-8") as fh_w:
                    for fasta_line in fh:
                        fh_w.write(fasta_line)

    fasta_generator = Metadata.fasta_iter(tmp_fasta_file)
    seq_data = {}
    for entry in fasta_generator:
        header, seq = entry
        if(header not in seq_regions):
            print(("ERROR: Fasta header not found among sequence region IDs." +
                  "\nFASTA header: " + header), file=sys.stderr)
            quit(1)

        seq_data[header] = seq

    gff3_tmp_str = {}
    # Initialize dict of strings
    for seq_region in seq_regions:
        gff3_tmp_str[seq_region] = ""

    # Annotation bug patterns
    re_att_bug_note = re.compile(r"(.*[^;])(note=.+)")
    re_att_bug_product = re.compile(r"(.*[^;])(product=.+)")

    # EMBOSS seqret fails if called with the -feature flag if none exists
    has_features = {}

    # Load all the gff entries into region specific strings.
    with open(args.gff3_file, "r") as fh:
        for line in fh:
            if(line.startswith("##FASTA")):
                break
            elif(line.startswith("#")):
                continue

            line = line.rstrip()

            line_list = line.split("\t")

            has_features[line_list[0]] = True

            attributes = line_list[-1]
            # Will decode possible URL encoded string.
            attributes = urllib.parse.unquote(line_list[-1])
            # Sometimes whitespace will be represented as + signs.
            attributes = attributes.replace('+', ' ')

            # Correcting known bugs in annotation.
            match_att_bug_note = re_att_bug_note.search(attributes)
            if(match_att_bug_note):
                left = match_att_bug_note.group(1)
                right = match_att_bug_note.group(2)
                attributes = left + ";" + right

            match_att_bug_product = re_att_bug_product.search(attributes)
            if(match_att_bug_product):
                left = match_att_bug_product.group(1)
                right = match_att_bug_product.group(2)
                attributes = left + ";" + right

            line_list = line_list[:-1]
            line_list.append(attributes)
            line = "\t".join(line_list)

            gff3_tmp_str[line_list[0]] += line + "\n"

    # Create final content of temporary individual GFF3 files
    for seq_region in seq_regions:
        complete_file = "##gff-version 3\n"
        start, end = seq_regions[seq_region]
        complete_file += ("##sequence-region " + seq_region + " " + str(start) +
                          " " + str(end) + "\n")
        complete_file += gff3_tmp_str[seq_region]
        complete_file += "##FASTA\n"
        complete_file += ">" + seq_region + "\n"
        complete_file += seq_data[seq_region] + "\n"
        gff3_tmp_str[seq_region] = complete_file

    # Write each region specific string to individual files
    gff3_tmp_files = {}
    for i, seq_region in enumerate(seq_regions):
        tmp_gff_file = temp_dir + "/gff3_" + str(i)
        tmp_embl_file = temp_dir + "/embl_" + str(i)
        gff3_tmp_files[seq_region] = (tmp_gff_file, tmp_embl_file)
        with open(tmp_gff_file, "w", encoding="utf-8") as fh:
            fh.write(gff3_tmp_str[seq_region])

    # Convert each gff3 file to embl format.
    emboss_log = temp_dir + "/emboss_log"
    for seq_region in seq_regions:
        gff3_file, embl_file = gff3_tmp_files[seq_region]
        if(seq_region in has_features):
            subprocess.run((prgs["seqret"] + " -feature" +
                            " gff3::" + gff3_file +
                            " embl::" + embl_file), shell=True)
        else:
            subprocess.run((prgs["seqret"] +
                            " gff3::" + gff3_file +
                            " embl::" + embl_file), shell=True)

    # Write each embl file to output file.
    # The header of each embl file is replaced by the one created previously.
    with open(args.output, "w", encoding="utf-8") as fh_out:

        # Cathces a bug in EMBOSS that alters this entry because it is deemed
        # illigal.
        re_coord_bug = re.compile(
            r"(.+)\/note=\"\*inference: COORDINATES:profile:(.+)")

        for seq_region in seq_regions:
            start, end = seq_regions[seq_region]
            embl_header = get_header(args.genome_type, seq_region, start, end,
                                     args.project_acc, args.authors, args.title,
                                     args.submission_date, args.organism,
                                     args.taxon_id)
            gff3_file, embl_file = gff3_tmp_files[seq_region]
            with open(embl_file, "r") as fh:
                # Reads header
                for line in fh:
                    # Also handles the case with no features "SQ"
                    if(line.startswith("FT") or line.startswith("SQ")):
                        fh_out.write(embl_header)
                        break
                # First line of features or sequence
                fh_out.write(line)
                # Reads features and/or sequence
                for line in fh:
                    match_coord_bug = re_coord_bug.search(line)
                    if(match_coord_bug):
                        line = line.rstrip()
                        left = match_coord_bug.group(1)
                        right = match_coord_bug.group(2)
                        line = (left + "/inference=\"COORDINATES:profile:" +
                                right + "\n")
                        fh_out.write(line)
                    else:
                        fh_out.write(line)

    subprocess.run(prgs["java"] + " -jar " + prgs['embl-api-validator'] +
                   " -r " + args.output, shell=True)

    clean()

    quit(0)
